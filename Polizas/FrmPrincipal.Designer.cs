﻿namespace Polizas
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.tcPolizas = new System.Windows.Forms.TabControl();
            this.tpDatosUsuario = new System.Windows.Forms.TabPage();
            this.gbUsuario = new System.Windows.Forms.GroupBox();
            this.gbClientes = new System.Windows.Forms.GroupBox();
            this.dgvClientes = new System.Windows.Forms.DataGridView();
            this.gbControls = new System.Windows.Forms.GroupBox();
            this.btnRefrescar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.pbFondoDatosUsr = new System.Windows.Forms.PictureBox();
            this.tpRegistroPoliza = new System.Windows.Forms.TabPage();
            this.gbRegistro = new System.Windows.Forms.GroupBox();
            this.gbResto = new System.Windows.Forms.GroupBox();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.tbMontoAsegurado = new System.Windows.Forms.TextBox();
            this.lblMontoAsegurado = new System.Windows.Forms.Label();
            this.lblPrimaPorPagar = new System.Windows.Forms.Label();
            this.tbPrimaPorPagar = new System.Windows.Forms.TextBox();
            this.dgvCoberturas = new System.Windows.Forms.DataGridView();
            this.gbAdicciones = new System.Windows.Forms.GroupBox();
            this.lvAdicciones = new System.Windows.Forms.ListView();
            this.gbCoberturas = new System.Windows.Forms.GroupBox();
            this.lvCoberturas = new System.Windows.Forms.ListView();
            this.gbCliente = new System.Windows.Forms.GroupBox();
            this.tbEdad = new System.Windows.Forms.TextBox();
            this.tbGenero = new System.Windows.Forms.TextBox();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.tbCedula = new System.Windows.Forms.TextBox();
            this.lblGenero = new System.Windows.Forms.Label();
            this.lblEdad = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblCedula = new System.Windows.Forms.Label();
            this.pbFondoRegPoliza = new System.Windows.Forms.PictureBox();
            this.pControls = new System.Windows.Forms.Panel();
            this.btnSalir = new System.Windows.Forms.Button();
            this.tcPolizas.SuspendLayout();
            this.tpDatosUsuario.SuspendLayout();
            this.gbUsuario.SuspendLayout();
            this.gbClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).BeginInit();
            this.gbControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFondoDatosUsr)).BeginInit();
            this.tpRegistroPoliza.SuspendLayout();
            this.gbRegistro.SuspendLayout();
            this.gbResto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCoberturas)).BeginInit();
            this.gbAdicciones.SuspendLayout();
            this.gbCoberturas.SuspendLayout();
            this.gbCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFondoRegPoliza)).BeginInit();
            this.pControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcPolizas
            // 
            this.tcPolizas.Controls.Add(this.tpDatosUsuario);
            this.tcPolizas.Controls.Add(this.tpRegistroPoliza);
            this.tcPolizas.Font = new System.Drawing.Font("Microsoft JhengHei UI Light", 14F);
            this.tcPolizas.Location = new System.Drawing.Point(12, 12);
            this.tcPolizas.Name = "tcPolizas";
            this.tcPolizas.SelectedIndex = 0;
            this.tcPolizas.Size = new System.Drawing.Size(651, 633);
            this.tcPolizas.TabIndex = 0;
            // 
            // tpDatosUsuario
            // 
            this.tpDatosUsuario.Controls.Add(this.gbUsuario);
            this.tpDatosUsuario.Controls.Add(this.pbFondoDatosUsr);
            this.tpDatosUsuario.ForeColor = System.Drawing.Color.YellowGreen;
            this.tpDatosUsuario.Location = new System.Drawing.Point(4, 33);
            this.tpDatosUsuario.Name = "tpDatosUsuario";
            this.tpDatosUsuario.Padding = new System.Windows.Forms.Padding(3);
            this.tpDatosUsuario.Size = new System.Drawing.Size(643, 596);
            this.tpDatosUsuario.TabIndex = 0;
            this.tpDatosUsuario.Text = "Datos de Usuario";
            this.tpDatosUsuario.UseVisualStyleBackColor = true;
            // 
            // gbUsuario
            // 
            this.gbUsuario.Controls.Add(this.gbClientes);
            this.gbUsuario.Controls.Add(this.gbControls);
            this.gbUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbUsuario.Font = new System.Drawing.Font("Microsoft JhengHei UI Light", 14F);
            this.gbUsuario.ForeColor = System.Drawing.Color.YellowGreen;
            this.gbUsuario.Location = new System.Drawing.Point(0, 0);
            this.gbUsuario.Name = "gbUsuario";
            this.gbUsuario.Size = new System.Drawing.Size(637, 590);
            this.gbUsuario.TabIndex = 3;
            this.gbUsuario.TabStop = false;
            // 
            // gbClientes
            // 
            this.gbClientes.Controls.Add(this.dgvClientes);
            this.gbClientes.ForeColor = System.Drawing.Color.YellowGreen;
            this.gbClientes.Location = new System.Drawing.Point(12, 141);
            this.gbClientes.Name = "gbClientes";
            this.gbClientes.Size = new System.Drawing.Size(613, 424);
            this.gbClientes.TabIndex = 10;
            this.gbClientes.TabStop = false;
            this.gbClientes.Text = "Clientes";
            // 
            // dgvClientes
            // 
            this.dgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClientes.Location = new System.Drawing.Point(6, 30);
            this.dgvClientes.Name = "dgvClientes";
            this.dgvClientes.Size = new System.Drawing.Size(601, 388);
            this.dgvClientes.TabIndex = 0;
            // 
            // gbControls
            // 
            this.gbControls.Controls.Add(this.btnRefrescar);
            this.gbControls.Controls.Add(this.btnBorrar);
            this.gbControls.Controls.Add(this.btnEditar);
            this.gbControls.Controls.Add(this.btnNuevo);
            this.gbControls.Location = new System.Drawing.Point(12, 30);
            this.gbControls.Name = "gbControls";
            this.gbControls.Size = new System.Drawing.Size(613, 105);
            this.gbControls.TabIndex = 1;
            this.gbControls.TabStop = false;
            // 
            // btnRefrescar
            // 
            this.btnRefrescar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefrescar.ForeColor = System.Drawing.Color.YellowGreen;
            this.btnRefrescar.Location = new System.Drawing.Point(459, 30);
            this.btnRefrescar.Name = "btnRefrescar";
            this.btnRefrescar.Size = new System.Drawing.Size(145, 55);
            this.btnRefrescar.TabIndex = 9;
            this.btnRefrescar.Text = "Refrescar";
            this.btnRefrescar.UseVisualStyleBackColor = true;
            // 
            // btnBorrar
            // 
            this.btnBorrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBorrar.ForeColor = System.Drawing.Color.YellowGreen;
            this.btnBorrar.Location = new System.Drawing.Point(308, 30);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(145, 55);
            this.btnBorrar.TabIndex = 8;
            this.btnBorrar.Text = "Borrar";
            this.btnBorrar.UseVisualStyleBackColor = true;
            // 
            // btnEditar
            // 
            this.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditar.ForeColor = System.Drawing.Color.YellowGreen;
            this.btnEditar.Location = new System.Drawing.Point(157, 30);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(145, 55);
            this.btnEditar.TabIndex = 7;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            // 
            // btnNuevo
            // 
            this.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevo.ForeColor = System.Drawing.Color.YellowGreen;
            this.btnNuevo.Location = new System.Drawing.Point(6, 30);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(145, 55);
            this.btnNuevo.TabIndex = 6;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            // 
            // pbFondoDatosUsr
            // 
            this.pbFondoDatosUsr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbFondoDatosUsr.Image = ((System.Drawing.Image)(resources.GetObject("pbFondoDatosUsr.Image")));
            this.pbFondoDatosUsr.Location = new System.Drawing.Point(3, 3);
            this.pbFondoDatosUsr.Name = "pbFondoDatosUsr";
            this.pbFondoDatosUsr.Size = new System.Drawing.Size(637, 590);
            this.pbFondoDatosUsr.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFondoDatosUsr.TabIndex = 0;
            this.pbFondoDatosUsr.TabStop = false;
            // 
            // tpRegistroPoliza
            // 
            this.tpRegistroPoliza.Controls.Add(this.gbRegistro);
            this.tpRegistroPoliza.Controls.Add(this.pbFondoRegPoliza);
            this.tpRegistroPoliza.Location = new System.Drawing.Point(4, 33);
            this.tpRegistroPoliza.Name = "tpRegistroPoliza";
            this.tpRegistroPoliza.Padding = new System.Windows.Forms.Padding(3);
            this.tpRegistroPoliza.Size = new System.Drawing.Size(643, 596);
            this.tpRegistroPoliza.TabIndex = 1;
            this.tpRegistroPoliza.Text = "Registro de Póliza";
            this.tpRegistroPoliza.UseVisualStyleBackColor = true;
            // 
            // gbRegistro
            // 
            this.gbRegistro.Controls.Add(this.gbResto);
            this.gbRegistro.Controls.Add(this.gbAdicciones);
            this.gbRegistro.Controls.Add(this.gbCoberturas);
            this.gbRegistro.Controls.Add(this.gbCliente);
            this.gbRegistro.ForeColor = System.Drawing.Color.YellowGreen;
            this.gbRegistro.Location = new System.Drawing.Point(0, 6);
            this.gbRegistro.Name = "gbRegistro";
            this.gbRegistro.Size = new System.Drawing.Size(637, 584);
            this.gbRegistro.TabIndex = 1;
            this.gbRegistro.TabStop = false;
            // 
            // gbResto
            // 
            this.gbResto.Controls.Add(this.btnCalcular);
            this.gbResto.Controls.Add(this.tbMontoAsegurado);
            this.gbResto.Controls.Add(this.lblMontoAsegurado);
            this.gbResto.Controls.Add(this.lblPrimaPorPagar);
            this.gbResto.Controls.Add(this.tbPrimaPorPagar);
            this.gbResto.Controls.Add(this.dgvCoberturas);
            this.gbResto.ForeColor = System.Drawing.Color.YellowGreen;
            this.gbResto.Location = new System.Drawing.Point(6, 355);
            this.gbResto.Name = "gbResto";
            this.gbResto.Size = new System.Drawing.Size(619, 223);
            this.gbResto.TabIndex = 3;
            this.gbResto.TabStop = false;
            this.gbResto.Text = "Info";
            // 
            // btnCalcular
            // 
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.ForeColor = System.Drawing.Color.YellowGreen;
            this.btnCalcular.Location = new System.Drawing.Point(259, 42);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(99, 54);
            this.btnCalcular.TabIndex = 5;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            // 
            // tbMontoAsegurado
            // 
            this.tbMontoAsegurado.Location = new System.Drawing.Point(16, 54);
            this.tbMontoAsegurado.Name = "tbMontoAsegurado";
            this.tbMontoAsegurado.Size = new System.Drawing.Size(237, 31);
            this.tbMontoAsegurado.TabIndex = 4;
            // 
            // lblMontoAsegurado
            // 
            this.lblMontoAsegurado.AutoSize = true;
            this.lblMontoAsegurado.ForeColor = System.Drawing.Color.YellowGreen;
            this.lblMontoAsegurado.Location = new System.Drawing.Point(12, 27);
            this.lblMontoAsegurado.Name = "lblMontoAsegurado";
            this.lblMontoAsegurado.Size = new System.Drawing.Size(166, 24);
            this.lblMontoAsegurado.TabIndex = 3;
            this.lblMontoAsegurado.Text = "Monto asegurado";
            // 
            // lblPrimaPorPagar
            // 
            this.lblPrimaPorPagar.AutoSize = true;
            this.lblPrimaPorPagar.ForeColor = System.Drawing.Color.YellowGreen;
            this.lblPrimaPorPagar.Location = new System.Drawing.Point(273, 184);
            this.lblPrimaPorPagar.Name = "lblPrimaPorPagar";
            this.lblPrimaPorPagar.Size = new System.Drawing.Size(132, 24);
            this.lblPrimaPorPagar.TabIndex = 2;
            this.lblPrimaPorPagar.Text = "Prima a pagar";
            // 
            // tbPrimaPorPagar
            // 
            this.tbPrimaPorPagar.Location = new System.Drawing.Point(411, 181);
            this.tbPrimaPorPagar.Name = "tbPrimaPorPagar";
            this.tbPrimaPorPagar.Size = new System.Drawing.Size(202, 31);
            this.tbPrimaPorPagar.TabIndex = 1;
            // 
            // dgvCoberturas
            // 
            this.dgvCoberturas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCoberturas.Location = new System.Drawing.Point(363, 20);
            this.dgvCoberturas.Name = "dgvCoberturas";
            this.dgvCoberturas.Size = new System.Drawing.Size(250, 155);
            this.dgvCoberturas.TabIndex = 0;
            // 
            // gbAdicciones
            // 
            this.gbAdicciones.Controls.Add(this.lvAdicciones);
            this.gbAdicciones.ForeColor = System.Drawing.Color.YellowGreen;
            this.gbAdicciones.Location = new System.Drawing.Point(369, 141);
            this.gbAdicciones.Name = "gbAdicciones";
            this.gbAdicciones.Size = new System.Drawing.Size(256, 208);
            this.gbAdicciones.TabIndex = 2;
            this.gbAdicciones.TabStop = false;
            this.gbAdicciones.Text = "Adicciones";
            // 
            // lvAdicciones
            // 
            this.lvAdicciones.Location = new System.Drawing.Point(6, 30);
            this.lvAdicciones.Name = "lvAdicciones";
            this.lvAdicciones.Size = new System.Drawing.Size(244, 172);
            this.lvAdicciones.TabIndex = 0;
            this.lvAdicciones.UseCompatibleStateImageBehavior = false;
            // 
            // gbCoberturas
            // 
            this.gbCoberturas.Controls.Add(this.lvCoberturas);
            this.gbCoberturas.ForeColor = System.Drawing.Color.YellowGreen;
            this.gbCoberturas.Location = new System.Drawing.Point(6, 141);
            this.gbCoberturas.Name = "gbCoberturas";
            this.gbCoberturas.Size = new System.Drawing.Size(357, 208);
            this.gbCoberturas.TabIndex = 1;
            this.gbCoberturas.TabStop = false;
            this.gbCoberturas.Text = "Coberturas";
            // 
            // lvCoberturas
            // 
            this.lvCoberturas.Location = new System.Drawing.Point(9, 30);
            this.lvCoberturas.Name = "lvCoberturas";
            this.lvCoberturas.Size = new System.Drawing.Size(342, 172);
            this.lvCoberturas.TabIndex = 1;
            this.lvCoberturas.UseCompatibleStateImageBehavior = false;
            // 
            // gbCliente
            // 
            this.gbCliente.Controls.Add(this.tbEdad);
            this.gbCliente.Controls.Add(this.tbGenero);
            this.gbCliente.Controls.Add(this.tbNombre);
            this.gbCliente.Controls.Add(this.tbCedula);
            this.gbCliente.Controls.Add(this.lblGenero);
            this.gbCliente.Controls.Add(this.lblEdad);
            this.gbCliente.Controls.Add(this.lblNombre);
            this.gbCliente.Controls.Add(this.lblCedula);
            this.gbCliente.ForeColor = System.Drawing.Color.YellowGreen;
            this.gbCliente.Location = new System.Drawing.Point(6, 30);
            this.gbCliente.Name = "gbCliente";
            this.gbCliente.Size = new System.Drawing.Size(619, 105);
            this.gbCliente.TabIndex = 0;
            this.gbCliente.TabStop = false;
            this.gbCliente.Text = "Cliente";
            // 
            // tbEdad
            // 
            this.tbEdad.Location = new System.Drawing.Point(447, 54);
            this.tbEdad.Name = "tbEdad";
            this.tbEdad.Size = new System.Drawing.Size(80, 31);
            this.tbEdad.TabIndex = 7;
            // 
            // tbGenero
            // 
            this.tbGenero.Location = new System.Drawing.Point(533, 54);
            this.tbGenero.Name = "tbGenero";
            this.tbGenero.Size = new System.Drawing.Size(80, 31);
            this.tbGenero.TabIndex = 6;
            // 
            // tbNombre
            // 
            this.tbNombre.Location = new System.Drawing.Point(174, 54);
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.Size = new System.Drawing.Size(267, 31);
            this.tbNombre.TabIndex = 5;
            // 
            // tbCedula
            // 
            this.tbCedula.Location = new System.Drawing.Point(10, 54);
            this.tbCedula.Name = "tbCedula";
            this.tbCedula.Size = new System.Drawing.Size(158, 31);
            this.tbCedula.TabIndex = 4;
            // 
            // lblGenero
            // 
            this.lblGenero.AutoSize = true;
            this.lblGenero.Location = new System.Drawing.Point(529, 27);
            this.lblGenero.Name = "lblGenero";
            this.lblGenero.Size = new System.Drawing.Size(74, 24);
            this.lblGenero.TabIndex = 3;
            this.lblGenero.Text = "Género";
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.Location = new System.Drawing.Point(443, 27);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(54, 24);
            this.lblEdad.TabIndex = 2;
            this.lblEdad.Text = "Edad";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(170, 27);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(83, 24);
            this.lblNombre.TabIndex = 1;
            this.lblNombre.Text = "Nombre";
            // 
            // lblCedula
            // 
            this.lblCedula.AutoSize = true;
            this.lblCedula.Location = new System.Drawing.Point(6, 27);
            this.lblCedula.Name = "lblCedula";
            this.lblCedula.Size = new System.Drawing.Size(70, 24);
            this.lblCedula.TabIndex = 0;
            this.lblCedula.Text = "Cédula";
            // 
            // pbFondoRegPoliza
            // 
            this.pbFondoRegPoliza.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbFondoRegPoliza.Image = global::Polizas.Properties.Resources.FondoPoliza;
            this.pbFondoRegPoliza.Location = new System.Drawing.Point(3, 3);
            this.pbFondoRegPoliza.Name = "pbFondoRegPoliza";
            this.pbFondoRegPoliza.Size = new System.Drawing.Size(637, 590);
            this.pbFondoRegPoliza.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFondoRegPoliza.TabIndex = 0;
            this.pbFondoRegPoliza.TabStop = false;
            // 
            // pControls
            // 
            this.pControls.Controls.Add(this.btnSalir);
            this.pControls.Location = new System.Drawing.Point(16, 651);
            this.pControls.Name = "pControls";
            this.pControls.Size = new System.Drawing.Size(643, 54);
            this.pControls.TabIndex = 1;
            // 
            // btnSalir
            // 
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.ForeColor = System.Drawing.Color.YellowGreen;
            this.btnSalir.Location = new System.Drawing.Point(545, 18);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(92, 33);
            this.btnSalir.TabIndex = 0;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 717);
            this.Controls.Add(this.pControls);
            this.Controls.Add(this.tcPolizas);
            this.ForeColor = System.Drawing.Color.YellowGreen;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmPrincipal";
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.tcPolizas.ResumeLayout(false);
            this.tpDatosUsuario.ResumeLayout(false);
            this.gbUsuario.ResumeLayout(false);
            this.gbClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).EndInit();
            this.gbControls.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbFondoDatosUsr)).EndInit();
            this.tpRegistroPoliza.ResumeLayout(false);
            this.gbRegistro.ResumeLayout(false);
            this.gbResto.ResumeLayout(false);
            this.gbResto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCoberturas)).EndInit();
            this.gbAdicciones.ResumeLayout(false);
            this.gbCoberturas.ResumeLayout(false);
            this.gbCliente.ResumeLayout(false);
            this.gbCliente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFondoRegPoliza)).EndInit();
            this.pControls.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcPolizas;
        private System.Windows.Forms.Panel pControls;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.TabPage tpDatosUsuario;
        private System.Windows.Forms.PictureBox pbFondoDatosUsr;
        private System.Windows.Forms.TabPage tpRegistroPoliza;
        private System.Windows.Forms.GroupBox gbRegistro;
        private System.Windows.Forms.GroupBox gbResto;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.TextBox tbMontoAsegurado;
        private System.Windows.Forms.Label lblMontoAsegurado;
        private System.Windows.Forms.Label lblPrimaPorPagar;
        private System.Windows.Forms.TextBox tbPrimaPorPagar;
        private System.Windows.Forms.DataGridView dgvCoberturas;
        private System.Windows.Forms.GroupBox gbAdicciones;
        private System.Windows.Forms.ListView lvAdicciones;
        private System.Windows.Forms.GroupBox gbCoberturas;
        private System.Windows.Forms.ListView lvCoberturas;
        private System.Windows.Forms.GroupBox gbCliente;
        private System.Windows.Forms.TextBox tbEdad;
        private System.Windows.Forms.TextBox tbGenero;
        private System.Windows.Forms.TextBox tbNombre;
        private System.Windows.Forms.TextBox tbCedula;
        private System.Windows.Forms.Label lblGenero;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblCedula;
        private System.Windows.Forms.PictureBox pbFondoRegPoliza;
        private System.Windows.Forms.GroupBox gbUsuario;
        private System.Windows.Forms.GroupBox gbClientes;
        private System.Windows.Forms.DataGridView dgvClientes;
        private System.Windows.Forms.GroupBox gbControls;
        private System.Windows.Forms.Button btnRefrescar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnNuevo;
    }
}