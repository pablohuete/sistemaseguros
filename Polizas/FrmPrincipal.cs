﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polizas
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            CargarApariencia();
        }

        private void CargarApariencia()
        {
            // Carga el color de fondo del Form.
            this.BackColor = ColorTranslator.FromHtml("#43335A");

            // -------------------- Pestaña "Datos de Clientes" -------------------- //

            // Color de fondo de la pestaña Datos Usuario.
            tpDatosUsuario.BackColor = ColorTranslator.FromHtml("#231D2E");

            // Fondo de GroupBox generales opaco.
            gbUsuario.BackColor = Color.FromArgb(140, 0, 0, 0);

            // Define el Parent del componente para que adopte el fondo.
            gbUsuario.Parent = pbFondoDatosUsr;
            gbClientes.Parent = gbUsuario;
            gbControls.Parent = gbUsuario;

            // Fondo de componentes detro de GroupBox transparentes, para que tomen el color del Parent.
            gbClientes.BackColor = Color.Transparent;
            gbControls.BackColor = Color.Transparent;

            // -------------------- Pestaña "Registro de Póliza" -------------------- //
            gbRegistro.BackColor = ColorTranslator.FromHtml("#231D2E");

            // Color de fondo de la pestaña Registro Póliza.
            tpRegistroPoliza.BackColor = ColorTranslator.FromHtml("#231D2E");

            // Fondo de GroupBox generales opaco.
            gbRegistro.BackColor = Color.FromArgb(140, 0, 0, 0);
            gbCliente.BackColor = Color.FromArgb(140, 0, 0, 0);
            gbCoberturas.BackColor = Color.FromArgb(140, 0, 0, 0);
            gbAdicciones.BackColor = Color.FromArgb(140, 0, 0, 0);
            gbResto.BackColor = Color.FromArgb(140, 0, 0, 0);

            // Define el Parent del componente para que adopte el fondo.
            gbRegistro.Parent = pbFondoRegPoliza;
            gbCliente.Parent = gbRegistro;
            gbCoberturas.Parent = gbRegistro;
            gbAdicciones.Parent = gbRegistro;
            gbResto.Parent = gbRegistro;

            // Fondo de componentes detro de GroupBox transparentes, para que tomen el color del Parent.
            gbCliente.BackColor = Color.Transparent;
            gbCoberturas.BackColor = Color.Transparent;
            gbAdicciones.BackColor = Color.Transparent;
            gbResto.BackColor = Color.Transparent;
        }

        // -------------------- Pestaña "Datos de Clientes" -------------------- //

        private void btnNuevo_Click(object sender, EventArgs e)
        {

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {

        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {

        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {

        }

        // -------------------- Pestaña "Registro de Póliza" -------------------- //

        private void btnCalcular_Click(object sender, EventArgs e)
        {

        }

        // -------------------- Controles Generales -------------------- //

        private void btnPersonalizar_Click(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
